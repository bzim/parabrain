use std::env::var;

fn main() {
    if var("CARGO_CFG_TARGET_ARCH").unwrap() == "x86_64" {
        println!("cargo:rustc-cfg=has_parab_jit");
    }
}
