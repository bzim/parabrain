use std::{
    collections::VecDeque,
    io::{stdin, stdout, Read, Write},
};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Op {
    Inc(u16),
    Dec(u16),
    Next(usize),
    Prev(usize),
    In,
    Out,
    Loop(Vec<Op>),
}

pub trait Eval {
    fn eval(&self);
}

impl Eval for [Op] {
    fn eval(mut self: &Self) {
        let mut stack = Vec::new();
        let mut ip = 0;
        let mut cells = VecDeque::<u16>::new();
        cells.push_back(0);
        let mut cell_ptr = 0;
        loop {
            let op = match self.get(ip) {
                Some(x) => x,
                None => if cells[cell_ptr] == 0 {
                    if let Some((new_self, new_ip)) = stack.pop() {
                        self = new_self;
                        ip = new_ip;
                        continue;
                    } else {
                        break;
                    }
                } else if stack.len() > 0 {
                    ip = 0;
                    continue;
                } else {
                    break;
                },
            };

            ip += 1;

            match op {
                &Op::Inc(x) => {
                    cells[cell_ptr] = cells[cell_ptr].wrapping_add(x)
                },

                &Op::Dec(x) => {
                    cells[cell_ptr] = cells[cell_ptr].wrapping_sub(x)
                },

                &Op::Next(x) => {
                    cell_ptr = cell_ptr.wrapping_add(x);
                    if cells.len() <= cell_ptr {
                        cells.resize(cell_ptr + 1, 0);
                    }
                },

                &Op::Prev(x) => {
                    if let Some(extra) = x.checked_sub(cell_ptr) {
                        cells.reserve(extra);
                        while cell_ptr < x {
                            cell_ptr += 1;
                            cells.push_front(0);
                        }
                        cell_ptr = 0;
                    } else {
                        cell_ptr -= x;
                    }
                },

                Op::In => {
                    let mut buf = [0];
                    match stdin().read_exact(&mut buf) {
                        Ok(()) => cells[cell_ptr] = buf[0] as u16,
                        Err(_) => cells[cell_ptr] = !0,
                    }
                },

                Op::Out => {
                    stdout().write_all(&[cells[cell_ptr] as u8]).unwrap();
                },

                Op::Loop(v) => {
                    if cells[cell_ptr] != 0 {
                        stack.push((self, ip));
                        self = &**v;
                        ip = 0;
                    }
                },
            }
        }
    }
}
