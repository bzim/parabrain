extern crate parabrain;

use parabrain::{
    compilation::Compilation,
    eval::{Eval, Op},
    parser::parse,
};

#[cfg(feature = "parab_jit")]
use parabrain::jit::Native;

use std::{env::args, fs::File, io::Read, process};

fn main() {
    let mut args = args();
    args.next();
    let path = match args.next() {
        Some(p) => p,
        _ => {
            eprintln!("error: expected input file argument");
            process::exit(-1);
        },
    };
    if let Some(x) = args.next() {
        eprintln!("error: unexpected argument {}", x);
        process::exit(-1);
    }

    let mut file = match File::open(&path) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("error on {}: {}", path, e);
            process::exit(-1);
        },
    };
    let mut source = Vec::new();
    if let Err(e) = file.read_to_end(&mut source) {
        eprintln!("error on {}: {}", path, e);
        process::exit(-1);
    }

    let ast = match parse(&source) {
        Ok(ast) => ast,
        Err(e) => {
            eprintln!("parse error: {}", e);
            process::exit(-1);
        }
    };

    let ops: Vec<Op> = ast.compile();
    #[cfg(feature = "parab_jit")]
    {
        let native: Native = ops.compile();
        native.eval();
    }
    #[cfg(not(feature = "parab_jit"))]
    {
        ops.eval();
    }
}
