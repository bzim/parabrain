#![cfg_attr(feature = "parab_jit", feature(plugin))]
#![cfg_attr(feature = "parab_jit", plugin(dynasm))]

#[cfg(feature = "parab_jit")]
extern crate dynasmrt;

pub mod compilation;
pub mod eval;
pub mod parser;

#[cfg(feature = "parab_jit")]
pub mod jit;
