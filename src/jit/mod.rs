#[cfg(target_arch = "x86_64")]
mod x86_64;

#[cfg(target_arch = "x86")]
mod x86;

mod exec_env;

use self::exec_env::ExecEnv;
use dynasmrt::{AssemblyOffset, ExecutableBuffer};
use eval::Eval;

#[derive(Debug)]
pub struct Native {
    buf: ExecutableBuffer,
    offset: AssemblyOffset,
}

impl Eval for Native {
    fn eval(&self) {
        let mut env = ExecEnv::new();
        let fun: fn(*mut ExecEnv) = unsafe {
            *(&self.buf.ptr(self.offset) as *const *const u8 as *const _)
        };
        fun(&mut env as *mut _);
    }
}
