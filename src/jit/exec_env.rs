use std::{
    io::{self, Read, Write},
    mem,
    panic,
    process,
};

struct Hook {
    fun: Box<Fn(&panic::PanicInfo) + Sync + Send + 'static>,
}

pub struct ExecEnv {
    vec: Vec<u16>,
    hook: Hook,
}

impl Hook {
    fn toggle_abort_panic(&mut self) {
        panic::set_hook(mem::replace(&mut self.fun, panic::take_hook()));
    }
}

impl ExecEnv {
    pub fn new() -> Self {
        Self {
            vec: Vec::new(),
            hook: Hook {
                fun: Box::new(|info| {
                    eprintln!("FFI panic: {}", info);
                    process::abort();
                }),
            },
        }
    }

    pub fn element(&mut self, pos: usize) -> &mut u16 {
        self.hook.toggle_abort_panic();
        if self.vec.len() <= pos {
            self.vec.resize(pos + 1, 0);
        }
        let res = unsafe { self.vec.get_unchecked_mut(pos) };
        self.hook.toggle_abort_panic();
        res
    }

    pub fn resize_front(&mut self, count: usize) {
        self.hook.toggle_abort_panic();
        let mut new_vec = Vec::with_capacity(self.vec.len() + count);
        for _ in 0..count {
            new_vec.push(0);
        }
        new_vec.append(&mut self.vec);
        self.vec = new_vec;
        self.hook.toggle_abort_panic();
    }

    pub fn putchar(&mut self, ch: u8) {
        self.hook.toggle_abort_panic();
        io::stdout().write_all(&[ch]).unwrap();
        self.hook.toggle_abort_panic();
    }

    pub fn getchar(&mut self) -> u16 {
        self.hook.toggle_abort_panic();
        let mut buf = [0];
        let ret = if io::stdin().read_exact(&mut buf).is_ok() {
            buf[0] as u16
        } else {
            !0
        };
        self.hook.toggle_abort_panic();
        ret
    }
}
