use super::{exec_env::ExecEnv, Native};
use compilation::Compilation;
use dynasmrt::{x86::Assembler, DynasmApi, DynasmLabelApi};
use eval::Op;

dynasm! {
    dummy; .arch x86
}

unsafe extern "cdecl" fn element(env: *mut ExecEnv, pos: usize) -> *mut u16 {
    (*env).element(pos) as *mut _
}

unsafe extern "cdecl" fn resize_front(env: *mut ExecEnv, count: usize) {
    (*env).resize_front(count)
}

unsafe extern "cdecl" fn putchar(env: *mut ExecEnv, ch: u8) {
    (*env).putchar(ch)
}

unsafe extern "cdecl" fn getchar(env: *mut ExecEnv) -> u16 {
    (*env).getchar()
}

impl Compilation<Native> for [Op] {
    fn compile(&self) -> Native {
        let mut stack = Vec::new();
        let mut iter = self.iter();
        let mut asm = Assembler::new().unwrap();
        let offset = asm.offset();
        dynasm! {
            asm
                ; push ebp
                ; push ebx
                ; push edi
                ; push esi
                ; mov ebp, esp
                ; mov ebx, [ebp + 20]
                ; mov edi, 0
                ; push edi
                ; push ebx
                ; mov eax, DWORD element as _
                ; call eax
                ; mov esi, eax
                ; add esp, 8
        }

        loop {
            let op = match iter.next() {
                Some(op) => op,
                None => if let Some((new_iter, start, end)) = stack.pop() {
                    iter = new_iter;
                    dynasm! {
                        asm
                            ; jmp =>start
                            ; =>end
                    }
                    continue;
                } else {
                    break;
                },
            };

            match op {
                &Op::Inc(x) => dynasm! {
                    asm
                        ; add WORD [esi], WORD x as _
                },

                &Op::Dec(x) => dynasm! {
                    asm
                        ; sub WORD [esi], WORD x as _
                },

                &Op::Next(x) => dynasm! {
                    asm
                        ; add edi, DWORD x as _
                        ; push edi
                        ; push ebx
                        ; mov eax, DWORD element as _
                        ; call eax
                        ; mov esi, eax
                        ; add esp, 8
                },

                &Op::Prev(x) => {
                    let no_resize = asm.new_dynamic_label();
                    dynasm! {
                        asm
                            ; sub edi, DWORD x as _
                            ; jno =>no_resize
                            ; mov edx, edi
                            ; neg edx
                            ; push edx
                            ; push ebx
                            ; mov eax, DWORD resize_front as _
                            ; call eax
                            ; mov edi, 0
                            ; add esp, 8
                            ; =>no_resize
                            ; push edi
                            ; push ebx
                            ; mov eax, DWORD element as _
                            ; call eax
                            ; mov esi, eax
                            ; add esp, 8
                    }
                },

                Op::In => dynasm! {
                    asm
                        ; push ebx
                        ; mov eax, DWORD getchar as _
                        ; call eax
                        ; mov [esi], ax
                        ; add esp, 4
                },

                Op::Out => dynasm! {
                    asm
                        ; push DWORD [esi]
                        ; push ebx
                        ; mov eax, DWORD putchar as _
                        ; call eax
                        ; add esp, 8
                },

                Op::Loop(slice) => {
                    let start = asm.new_dynamic_label();
                    let end = asm.new_dynamic_label();
                    dynasm! {
                        asm
                            ; =>start
                            ; cmp WORD [esi], 0
                            ; je =>end
                    }
                    stack.push((iter, start, end));
                    iter = slice.iter();
                },
            }
        }

        dynasm! {
            asm
                ; mov esp, ebp
                ; pop esi
                ; pop edi
                ; pop ebx
                ; pop ebp
                ; ret
        }

        Native {
            buf: asm.finalize().unwrap(),
            offset,
        }
    }
}
