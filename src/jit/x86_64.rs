use super::{exec_env::ExecEnv, Native};
use compilation::Compilation;
use dynasmrt::{x64::Assembler, DynasmApi, DynasmLabelApi};
use eval::Op;

dynasm! {
    dummy; .arch x64
}

unsafe extern "sysv64" fn element(env: *mut ExecEnv, pos: usize) -> *mut u16 {
    (*env).element(pos) as *mut _
}

unsafe extern "sysv64" fn resize_front(env: *mut ExecEnv, count: usize) {
    (*env).resize_front(count)
}

unsafe extern "sysv64" fn putchar(env: *mut ExecEnv, ch: u8) {
    (*env).putchar(ch)
}

unsafe extern "sysv64" fn getchar(env: *mut ExecEnv) -> u16 {
    (*env).getchar()
}

impl Compilation<Native> for [Op] {
    fn compile(&self) -> Native {
        let mut stack = Vec::new();
        let mut iter = self.iter();
        let mut asm = Assembler::new().unwrap();
        let offset = asm.offset();
        dynasm! {
            asm
                ; push rbp
                ; push r12
                ; push r13
                ; push r14
                ; mov rbp, rsp
                // stack alignment
                ; sub rsp, 8
                ; mov r12, rdi
                ; mov r13, 0
                ; mov rsi, r13
                ; mov rax, QWORD element as _
                ; call rax
                ; mov r14, rax
        }

        loop {
            let op = match iter.next() {
                Some(op) => op,
                None => if let Some((new_iter, start, end)) = stack.pop() {
                    iter = new_iter;
                    dynasm! {
                        asm
                            ; jmp =>start
                            ; =>end
                    }
                    continue;
                } else {
                    break;
                },
            };

            match op {
                &Op::Inc(x) => dynasm! {
                    asm
                        ; add WORD [r14], WORD x as _
                },

                &Op::Dec(x) => dynasm! {
                    asm
                        ; sub WORD [r14], WORD x as _
                },

                &Op::Next(x) => dynasm! {
                    asm
                        ; mov rax, QWORD x as _
                        ; add r13, rax
                        ; mov rdi, r12
                        ; mov rsi, r13
                        ; mov rax, QWORD element as _
                        ; call rax
                        ; mov r14, rax
                },

                &Op::Prev(x) => {
                    let no_resize = asm.new_dynamic_label();
                    dynasm! {
                        asm
                            ; mov rax, QWORD x as _
                            ; sub r13, rax
                            ; jno =>no_resize
                            ; mov rdi, r12
                            ; mov rsi, r13
                            ; neg rsi
                            ; mov rax, QWORD resize_front as _
                            ; call rax
                            ; mov r13, 0
                            ; =>no_resize
                            ; mov rdi, r12
                            ; mov rsi, r13
                            ; mov rax, QWORD element as _
                            ; call rax
                            ; mov r14, rax
                    }
                },

                Op::In => dynasm! {
                    asm
                        ; mov rdi, r12
                        ; mov rax, QWORD getchar as _
                        ; call rax
                        ; mov [r14], ax
                },

                Op::Out => dynasm! {
                    asm
                        ; mov rdi, r12
                        ; mov ax, [r14]
                        ; mov rsi, rax
                        ; mov rax, QWORD putchar as _
                        ; call rax
                },

                Op::Loop(slice) => {
                    let start = asm.new_dynamic_label();
                    let end = asm.new_dynamic_label();
                    dynasm! {
                        asm
                            ; =>start
                            ; cmp WORD [r14], 0
                            ; je =>end
                    }
                    stack.push((iter, start, end));
                    iter = slice.iter();
                },
            }
        }

        dynasm! {
            asm
                ; mov rsp, rbp
                ; pop r14
                ; pop r13
                ; pop r12
                ; pop rbp
                ; ret
        }

        Native {
            buf: asm.finalize().unwrap(),
            offset,
        }
    }
}
