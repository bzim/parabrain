pub trait Compilation<T> {
    fn compile(&self) -> T;
}
