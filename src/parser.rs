use compilation::Compilation;
use eval::Op;
use std::fmt;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ErrorKind {
    Unclosed,
    BadClose,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Error {
    pub kind: ErrorKind,
    pub pos: usize,
    pub line: usize,
    pub col: usize,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum AstNode {
    Inc,
    Dec,
    Next,
    Prev,
    In,
    Out,
    Loop(Vec<AstNode>),
}

impl fmt::Display for Error {
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmtr,
            "{} on line {} column {} (offset {})",
            match self.kind {
                ErrorKind::Unclosed => "unclosed loop",
                ErrorKind::BadClose => "closed unexisting loop",
            },
            self.line,
            self.col,
            self.pos
        )
    }
}

impl Compilation<Vec<Op>> for [AstNode] {
    fn compile(&self) -> Vec<Op> {
        let mut iter = self.iter().peekable();
        let mut ops = Vec::new();
        let mut stack = Vec::<(_, Vec<Op>)>::new();

        loop {
            let node = match iter.next() {
                Some(node) => node,
                None => if let Some((new_iter, mut new_ops)) = stack.pop() {
                    new_ops.push(Op::Loop(ops));
                    iter = new_iter;
                    ops = new_ops;
                    continue;
                } else {
                    break;
                },
            };

            match node {
                AstNode::Inc => {
                    let mut count = 1;
                    while let Some(&AstNode::Inc) = iter.peek() {
                        iter.next();
                        count += 1;
                    }
                    ops.push(Op::Inc(count));
                },

                AstNode::Dec => {
                    let mut count = 1;
                    while let Some(&AstNode::Dec) = iter.peek() {
                        iter.next();
                        count += 1;
                    }
                    ops.push(Op::Dec(count));
                },

                AstNode::Next => {
                    let mut count = 1;
                    while let Some(&AstNode::Next) = iter.peek() {
                        iter.next();
                        count += 1;
                    }
                    ops.push(Op::Next(count));
                },

                AstNode::Prev => {
                    let mut count = 1;
                    while let Some(&AstNode::Prev) = iter.peek() {
                        iter.next();
                        count += 1;
                    }
                    ops.push(Op::Prev(count));
                },

                AstNode::In => ops.push(Op::In),

                AstNode::Out => ops.push(Op::Out),

                AstNode::Loop(nodes) => {
                    stack.push((iter, ops));
                    ops = Vec::new();
                    iter = nodes.iter().peekable();
                },
            }
        }

        ops
    }
}

pub fn parse<S>(src: &S) -> Result<Vec<AstNode>, Error>
where
    S: AsRef<[u8]>,
{
    let mut iter = src.as_ref().iter();
    let mut stack = Vec::new();
    let mut nodes = Vec::new();
    let mut pos = 0;
    let mut line = 1;
    let mut col = 1;

    while let Some(&ch) = iter.next() {
        let curr_pos = pos;
        pos += 1;
        if ch == b'\n' {
            line += 1;
            col = 1;
            continue;
        }
        col += 1;
        if ch == b'+' {
            nodes.push(AstNode::Inc);
        } else if ch == b'-' {
            nodes.push(AstNode::Dec);
        } else if ch == b'>' {
            nodes.push(AstNode::Next);
        } else if ch == b'<' {
            nodes.push(AstNode::Prev);
        } else if ch == b',' {
            nodes.push(AstNode::In);
        } else if ch == b'.' {
            nodes.push(AstNode::Out);
        } else if ch == b'[' {
            stack.push((curr_pos, line, col, nodes));
            nodes = Vec::new();
        } else if ch == b']' {
            let (_, _, _, mut popped) = stack.pop().ok_or(Error {
                kind: ErrorKind::BadClose,
                pos: curr_pos,
                line,
                col,
            })?;
            popped.push(AstNode::Loop(nodes));
            nodes = popped;
        }
    }

    if let Some((pos, line, col, _)) = stack.pop() {
        Err(Error {
            kind: ErrorKind::Unclosed,
            pos,
            line,
            col,
        })
    } else {
        Ok(nodes)
    }
}
